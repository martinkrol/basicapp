package br.com.martinkrol.basicapp.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BasicRepository<Entity extends BasicEntity>
extends JpaRepository<Entity, Long>{

}
