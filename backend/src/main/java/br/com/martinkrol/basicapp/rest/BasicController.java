package br.com.martinkrol.basicapp.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.martinkrol.basicapp.model.BasicRepository;


@RestController
public class BasicController {
  
  @Autowired
  BasicRepository basicRepository;

  @RequestMapping(value = "/", method = RequestMethod.GET, produces={"application/json"})
  public ResponseEntity<Object> findOne() {
//    BasicEntity entity = new BasicEntity();
//    entity.setNome("Entidade 1");
//    basicRepository.save(entity);
    return new ResponseEntity<Object>(basicRepository.findOne(new Long(1)), HttpStatus.OK);
  }
}